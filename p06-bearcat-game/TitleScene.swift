//
//  TitleScene.swift
//  p06-bearcat-game
//
//  Created by Vasili Papastrat on 3/2/16.
//  Copyright © 2016 Vasili Papastrat. All rights reserved.
//

import Foundation
import AVFoundation
import SpriteKit

class TitleScene : SKScene{
    
    //MARK: - Data members
    var audioPlayer = AVAudioPlayer()
    
    var btStart : SKSpriteNode?  //start button
    var btRules : SKSpriteNode?  //rules button
    
    var topBorder : SKSpriteNode?
    var bottomBorder : SKSpriteNode?
    var leftBorder : SKSpriteNode?
    var rightBorder : SKSpriteNode?
    
    var background = SKSpriteNode(imageNamed: "Baxter_Main_Menu")
    
    var lastSpawnTimeInterval : NSTimeInterval = 0 //time difference between now and last time asteroid spawned
    var lastUpdateTimeInterval : NSTimeInterval = 0//time difference between now and last update
    var lastLoopTimeInterval : NSTimeInterval = 0//restarts the music when done
    
    //MARK: - functions
    override func didMoveToView(view: SKView) {
        //set up music
        audioPlayer = AVAudioPlayer()
        let url = NSURL.fileURLWithPath(NSBundle.mainBundle().pathForResource("title", ofType: "mp3")!)
        do{
            try audioPlayer = AVAudioPlayer(contentsOfURL: url, fileTypeHint: nil)
        }catch{
            print("Error with music")
        }
        audioPlayer.play()
        
        //configure borders
        topBorder = SKSpriteNode()
        topBorder!.size = CGSize(width: self.view!.bounds.width, height: 10)
        topBorder!.anchorPoint = CGPoint(x: 0, y: 1)
        topBorder!.color = SKColor.yellowColor()
        leftBorder = SKSpriteNode()
        leftBorder!.size = CGSize(width: 10, height: self.view!.bounds.height)
        leftBorder!.anchorPoint = CGPoint(x: 0, y: 0)
        leftBorder!.color = SKColor.yellowColor()
        bottomBorder = SKSpriteNode()
        bottomBorder!.size = CGSize(width: self.view!.bounds.width, height: 10)
        bottomBorder!.anchorPoint = CGPoint(x: 0, y: 0)
        bottomBorder!.color = SKColor.yellowColor()
        rightBorder = SKSpriteNode()
        rightBorder!.size = CGSize(width: 10, height: self.view!.bounds.height)
        rightBorder!.anchorPoint = CGPoint(x: 1, y: 0)
        rightBorder!.color = SKColor.yellowColor()
        
        //position borders
        topBorder!.position = CGPoint(x: 0.0, y: self.view!.bounds.height)
        bottomBorder!.position = CGPoint(x:0.0, y:0.0)
        leftBorder!.position = CGPoint(x: 0.0, y: 0.0)
        rightBorder!.position = CGPoint(x: self.view!.bounds.width, y: 0.0)
        
        //configure background
        background.position = CGPoint(x: frame.size.width / 2, y:frame.size.height / 2)
        background.size = CGSize(width: frame.width - 10, height: frame.height - 10)
        
        //set up buttons
        btStart = SKSpriteNode(imageNamed: "PlayButton")
        btStart!.position = CGPoint(x: frame.size.width / 2, y:(frame.size.height / 2) - 10)
        btRules = SKSpriteNode(imageNamed: "RulesButton")
        btRules!.position = CGPoint(x: frame.size.width / 2, y:(frame.size.height / 4) - 5)
        
        //make buttons smaller
        btStart!.xScale = 0.4
        btStart!.yScale = 0.4
        btRules!.xScale = 0.4
        btRules!.yScale = 0.4
        
        //make buttons appear on top of background
        btStart!.zPosition = 1.0
        btRules!.zPosition = 1.0
        
        //add everything to scene
        self.addChild(background)
        self.addChild(topBorder!)
        self.addChild(bottomBorder!)
        self.addChild(leftBorder!)
        self.addChild(rightBorder!)
        self.addChild(btStart!)
        self.addChild(btRules!)
    }
    /*
    transitionToRulesScreen
        Transitions to a new scene to display the rules
    */
    func transitionToRulesScreen(){
        audioPlayer.stop()
        let reveal = SKTransition.flipHorizontalWithDuration(0.5)
        let nextScreen = RulesScene(size: self.size)
        self.view!.presentScene(nextScreen, transition: reveal)
    }
    
    /*
    transitionToGame
        Leaves the title screen and starts the game
    */
    func transistionToGame(){
        audioPlayer.stop()
        let url = NSURL.fileURLWithPath(NSBundle.mainBundle().pathForResource("start", ofType: "mp3")!)
        do{
            try audioPlayer = AVAudioPlayer(contentsOfURL: url, fileTypeHint: nil)
        }catch{
            print("Error with music")
        }
        audioPlayer.currentTime = 7.6
        audioPlayer.play()
        //let sceneTransition = SKTransition.doorsCloseHorizontalWithDuration(2.0)
        let reveal = SKTransition.fadeWithDuration(4.0)
        let nextScreen = GameScene(size: self.view!.bounds.size)
        self.view!.presentScene(nextScreen, transition: reveal)
    }
    
    /* 
    update
    
    */
    override func update(currentTime: CFTimeInterval) {
        var timeSinceLast = currentTime - self.lastUpdateTimeInterval//get how much time passed since last update
        self.lastUpdateTimeInterval = currentTime //update data member
        if timeSinceLast > 1{//if more than a second has passed
            timeSinceLast = 1.0 / 60.0
            
        }
        self.updateWithTimeSinceLastUpdate(timeSinceLast)
    }
    
    /*
    touchesEnded
    
    */
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches{
            let location = touch.locationInNode(self)
            if btRules!.containsPoint(location){//if pressed rules button
                transitionToRulesScreen()
            }
            else if btStart!.containsPoint(location){
                transistionToGame()
            }
        }
    }
    
    /*
    updateWithTimeSinceLastUpdate
        Keeps track of ongoing time. Changes border colors to give flashing effect
    PARAMS:
        timeInterval
    */
    func updateWithTimeSinceLastUpdate(timeSinceLast : CFTimeInterval){
        self.lastSpawnTimeInterval += timeSinceLast
        self.lastLoopTimeInterval += timeSinceLast
        if self.lastSpawnTimeInterval > 0.15{//every 1/2 second...
            self.lastSpawnTimeInterval = 0//reset
            if topBorder!.color == SKColor.redColor(){
                topBorder!.color = SKColor.yellowColor()
                bottomBorder!.color = SKColor.yellowColor()
                leftBorder!.color = SKColor.yellowColor()
                rightBorder!.color = SKColor.yellowColor()
            }else if topBorder!.color == SKColor.yellowColor(){
                topBorder!.color = SKColor.whiteColor()
                bottomBorder!.color = SKColor.whiteColor()
                leftBorder!.color = SKColor.whiteColor()
                rightBorder!.color = SKColor.whiteColor()
            }else{
                topBorder!.color = SKColor.redColor()
                bottomBorder!.color = SKColor.redColor()
                leftBorder!.color = SKColor.redColor()
                rightBorder!.color = SKColor.redColor()
            }
        }
        if self.lastLoopTimeInterval > 46.00{
            if self.audioPlayer.playing{
                self.audioPlayer.stop()
            }
            self.audioPlayer.play()
            self.lastLoopTimeInterval = 0//reset
        }
    }
}