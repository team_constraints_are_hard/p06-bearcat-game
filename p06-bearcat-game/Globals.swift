//
//  Globals.swift
//  p06-bearcat-game
//
//  Created by Vasili Papastrat on 3/8/16.
//  Copyright © 2016 Vasili Papastrat. All rights reserved.
//

import Foundation
import SpriteKit

let baxterAtlas = SKTextureAtlas(named:"baxter.atlas")
let powerAtlas = SKTextureAtlas(named:"powerups.atlas")
let enemyAtlas = SKTextureAtlas(named:"enemy.atlas")

/*let walkLeftFrames = [baxterAtlas.textureNamed("BaxterWalkingLeft9.png"),
    baxterAtlas.textureNamed("BaxterWalkingLeft8.png"),
    baxterAtlas.textureNamed("BaxterWalkingLeft7.png"),
    baxterAtlas.textureNamed("BaxterWalkingLeft6.png"),
    baxterAtlas.textureNamed("BaxterWalkingLeft5.png"),
    baxterAtlas.textureNamed("BaxterWalkingLeft4.png"),
    baxterAtlas.textureNamed("BaxterWalkingLeft3.png"),
    baxterAtlas.textureNamed("BaxterWalkingLeft2.png"),
    baxterAtlas.textureNamed("BaxterWalkingLeft1.png")]*/

let walkLeftFrames = [baxterAtlas.textureNamed("BaxterWalkLeft1.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft2.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft3.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft4.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft5.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft6.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft7.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft8.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft9.png")]

let walkLeftFramesI = [baxterAtlas.textureNamed("BaxterWalkLeft1I.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft2I.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft3I.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft4I.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft5I.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft6I.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft7I.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft8I.png"),
    baxterAtlas.textureNamed("BaxterWalkLeft9I.png")]

/*let walkRightFrames = [baxterAtlas.textureNamed("BaxterWalkingRight1.png"),
    baxterAtlas.textureNamed("BaxterWalkingRight2.png"),
    baxterAtlas.textureNamed("BaxterWalkingRight3.png"),
    baxterAtlas.textureNamed("BaxterWalkingRight4.png"),
    baxterAtlas.textureNamed("BaxterWalkingRight5.png"),
    baxterAtlas.textureNamed("BaxterWalkingRight6.png"),
    baxterAtlas.textureNamed("BaxterWalkingRight7.png"),
    baxterAtlas.textureNamed("BaxterWalkingRight8.png"),
    baxterAtlas.textureNamed("BaxterWalkingRight9.png")]//make array of sprites to animate*/

let walkRightFrames = [baxterAtlas.textureNamed("BaxterWalkRight1.png"),
    baxterAtlas.textureNamed("BaxterWalkRight2.png"),
    baxterAtlas.textureNamed("BaxterWalkRight3.png"),
    baxterAtlas.textureNamed("BaxterWalkRight4.png"),
    baxterAtlas.textureNamed("BaxterWalkRight5.png"),
    baxterAtlas.textureNamed("BaxterWalkRight6.png"),
    baxterAtlas.textureNamed("BaxterWalkRight7.png"),
    baxterAtlas.textureNamed("BaxterWalkRight8.png"),
    baxterAtlas.textureNamed("BaxterWalkRight9.png")]//make array of sprites to animate

let walkRightFramesI = [baxterAtlas.textureNamed("BaxterWalkRight1I.png"),
    baxterAtlas.textureNamed("BaxterWalkRight2I.png"),
    baxterAtlas.textureNamed("BaxterWalkRight3I.png"),
    baxterAtlas.textureNamed("BaxterWalkRight4I.png"),
    baxterAtlas.textureNamed("BaxterWalkRight5I.png"),
    baxterAtlas.textureNamed("BaxterWalkRight6I.png"),
    baxterAtlas.textureNamed("BaxterWalkRight7I.png"),
    baxterAtlas.textureNamed("BaxterWalkRight8I.png"),
    baxterAtlas.textureNamed("BaxterWalkRight9I.png")]//make array of sprites to animate

let enemyGirl1WalkFrames = [enemyAtlas.textureNamed("Girl1WalkLeft1.png"),
    enemyAtlas.textureNamed("Girl1WalkLeft2.png"),
    enemyAtlas.textureNamed("Girl1WalkLeft3.png"),
    enemyAtlas.textureNamed("Girl1WalkLeft4.png"),
    enemyAtlas.textureNamed("Girl1WalkLeft5.png"),
    enemyAtlas.textureNamed("Girl1WalkLeft6.png"),
    enemyAtlas.textureNamed("Girl1WalkLeft7.png"),
    enemyAtlas.textureNamed("Girl1WalkLeft8.png"),
    enemyAtlas.textureNamed("Girl1WalkLeft9.png")]

let enemyGirl2WalkFrames = [enemyAtlas.textureNamed("Girl2WalkLeft1.png"),
    enemyAtlas.textureNamed("Girl2WalkLeft2.png"),
    enemyAtlas.textureNamed("Girl2WalkLeft3.png"),
    enemyAtlas.textureNamed("Girl2WalkLeft4.png"),
    enemyAtlas.textureNamed("Girl2WalkLeft5.png"),
    enemyAtlas.textureNamed("Girl2WalkLeft6.png"),
    enemyAtlas.textureNamed("Girl2WalkLeft7.png"),
    enemyAtlas.textureNamed("Girl2WalkLeft8.png"),
    enemyAtlas.textureNamed("Girl2WalkLeft9.png")]

let enemyGirl3WalkFrames = [enemyAtlas.textureNamed("Girl3WalkLeft1.png"),
    enemyAtlas.textureNamed("Girl3WalkLeft2.png"),
    enemyAtlas.textureNamed("Girl3WalkLeft3.png"),
    enemyAtlas.textureNamed("Girl3WalkLeft4.png"),
    enemyAtlas.textureNamed("Girl3WalkLeft5.png"),
    enemyAtlas.textureNamed("Girl3WalkLeft6.png"),
    enemyAtlas.textureNamed("Girl3WalkLeft7.png"),
    enemyAtlas.textureNamed("Girl3WalkLeft8.png"),
    enemyAtlas.textureNamed("Girl3WalkLeft9.png")]

let enemyBoy1WalkFrames = [enemyAtlas.textureNamed("Boy1WalkLeft1.png"),
    enemyAtlas.textureNamed("Boy1WalkLeft2.png"),
    enemyAtlas.textureNamed("Boy1WalkLeft3.png"),
    enemyAtlas.textureNamed("Boy1WalkLeft4.png"),
    enemyAtlas.textureNamed("Boy1WalkLeft5.png"),
    enemyAtlas.textureNamed("Boy1WalkLeft6.png"),
    enemyAtlas.textureNamed("Boy1WalkLeft7.png"),
    enemyAtlas.textureNamed("Boy1WalkLeft8.png"),
    enemyAtlas.textureNamed("Boy1WalkLeft9.png")]

let enemyBoy2WalkFrames = [enemyAtlas.textureNamed("Boy2WalkLeft1.png"),
    enemyAtlas.textureNamed("Boy2WalkLeft2.png"),
    enemyAtlas.textureNamed("Boy2WalkLeft3.png"),
    enemyAtlas.textureNamed("Boy2WalkLeft4.png"),
    enemyAtlas.textureNamed("Boy2WalkLeft5.png"),
    enemyAtlas.textureNamed("Boy2WalkLeft6.png"),
    enemyAtlas.textureNamed("Boy2WalkLeft7.png"),
    enemyAtlas.textureNamed("Boy2WalkLeft8.png"),
    enemyAtlas.textureNamed("Boy2WalkLeft9.png")]

let enemyBoy3WalkFrames = [enemyAtlas.textureNamed("Boy3WalkLeft1.png"),
    enemyAtlas.textureNamed("Boy3WalkLeft2.png"),
    enemyAtlas.textureNamed("Boy3WalkLeft3.png"),
    enemyAtlas.textureNamed("Boy3WalkLeft4.png"),
    enemyAtlas.textureNamed("Boy3WalkLeft5.png"),
    enemyAtlas.textureNamed("Boy3WalkLeft6.png"),
    enemyAtlas.textureNamed("Boy3WalkLeft7.png"),
    enemyAtlas.textureNamed("Boy3WalkLeft8.png"),
    enemyAtlas.textureNamed("Boy3WalkLeft9.png")]

/*let baxterPunchRightFrames = [baxterAtlas.textureNamed("BaxterPunchingRight5.png"),
    baxterAtlas.textureNamed("BaxterPunchingRight6.png"),
    baxterAtlas.textureNamed("BaxterPunchingRight3.png")]

let baxterPunchLeftFrames = [baxterAtlas.textureNamed("BaxterPunchingLeft5.png"),
baxterAtlas.textureNamed("BaxterPunchingLeft6.png"),
baxterAtlas.textureNamed("BaxterPunchingLeft3.png")]*/

let baxterPunchRightFrames = [baxterAtlas.textureNamed("BaxterPunchRight5.png"),
    baxterAtlas.textureNamed("BaxterPunchRight6.png"),
    baxterAtlas.textureNamed("BaxterPunchRight3.png")]

let baxterPunchLeftFrames = [baxterAtlas.textureNamed("BaxterPunchLeft5.png"),
    baxterAtlas.textureNamed("BaxterPunchLeft6.png"),
    baxterAtlas.textureNamed("BaxterPunchLeft3.png")]

let baxterPunchRightFramesI = [baxterAtlas.textureNamed("BaxterPunchRight5I.png"),
    baxterAtlas.textureNamed("BaxterPunchRight6I.png"),
    baxterAtlas.textureNamed("BaxterPunchRight3I.png")]

let baxterPunchLeftFramesI = [baxterAtlas.textureNamed("BaxterPunchLeft5I.png"),
    baxterAtlas.textureNamed("BaxterPunchLeft6I.png"),
    baxterAtlas.textureNamed("BaxterPunchLeft3I.png")]
/*

let baxterPunchRightFrames = [baxterAtlas.textureNamed("BaxterPunchingRight1.png"),
    baxterAtlas.textureNamed("BaxterPunchingRight2.png"),
    baxterAtlas.textureNamed("BaxterPunchingRight3.png"),
    baxterAtlas.textureNamed("BaxterPunchingRight4.png"),
    baxterAtlas.textureNamed("BaxterPunchingRight5.png"),
    baxterAtlas.textureNamed("BaxterPunchingRight6.png")]

let baxterPunchLeftFrames = [baxterAtlas.textureNamed("BaxterPunchingLeft1.png"),
    baxterAtlas.textureNamed("BaxterPunchingLeft2.png"),
    baxterAtlas.textureNamed("BaxterPunchingLeft3.png"),
    baxterAtlas.textureNamed("BaxterPunchingLeft4.png"),
    baxterAtlas.textureNamed("BaxterPunchingLeft5.png"),
    baxterAtlas.textureNamed("BaxterPunchingLeft6.png")]
*/
/*let baxterRightThrowFrames = [baxterAtlas.textureNamed("BaxterThrowingRight1"),
    baxterAtlas.textureNamed("BaxterThrowingRight2"),
    baxterAtlas.textureNamed("BaxterThrowingRight3"),
    baxterAtlas.textureNamed("BaxterThrowingRight4"),
    baxterAtlas.textureNamed("BaxterThrowingRight5"),
    baxterAtlas.textureNamed("BaxterThrowingRight6"),
    baxterAtlas.textureNamed("BaxterThrowingRight7"),
    baxterAtlas.textureNamed("BaxterThrowingRight8")]

let baxterLeftThrowFrames = [baxterAtlas.textureNamed("BaxterThrowingLeft1"),
    baxterAtlas.textureNamed("BaxterThrowingLeft2"),
    baxterAtlas.textureNamed("BaxterThrowingLeft3"),
    baxterAtlas.textureNamed("BaxterThrowingLeft4"),
    baxterAtlas.textureNamed("BaxterThrowingLeft5"),
    baxterAtlas.textureNamed("BaxterThrowingLeft6"),
    baxterAtlas.textureNamed("BaxterThrowingLeft7"),
    baxterAtlas.textureNamed("BaxterThrowingLeft8")]*/

let baxterRightThrowFrames = [baxterAtlas.textureNamed("BaxterThrowRight1"),
    baxterAtlas.textureNamed("BaxterThrowRight2"),
    baxterAtlas.textureNamed("BaxterThrowRight3"),
    baxterAtlas.textureNamed("BaxterThrowRight4"),
    baxterAtlas.textureNamed("BaxterThrowRight5"),
    baxterAtlas.textureNamed("BaxterThrowRight6"),
    baxterAtlas.textureNamed("BaxterThrowRight7"),
    baxterAtlas.textureNamed("BaxterThrowRight8")]

let baxterLeftThrowFrames = [baxterAtlas.textureNamed("BaxterThrowLeft1"),
    baxterAtlas.textureNamed("BaxterThrowLeft2"),
    baxterAtlas.textureNamed("BaxterThrowLeft3"),
    baxterAtlas.textureNamed("BaxterThrowLeft4"),
    baxterAtlas.textureNamed("BaxterThrowLeft5"),
    baxterAtlas.textureNamed("BaxterThrowLeft6"),
    baxterAtlas.textureNamed("BaxterThrowLeft7"),
    baxterAtlas.textureNamed("BaxterThrowLeft8")]

let baxterRightThrowFramesI = [baxterAtlas.textureNamed("BaxterThrowRight1I"),
    baxterAtlas.textureNamed("BaxterThrowRight2I"),
    baxterAtlas.textureNamed("BaxterThrowRight3I"),
    baxterAtlas.textureNamed("BaxterThrowRight4I"),
    baxterAtlas.textureNamed("BaxterThrowRight5I"),
    baxterAtlas.textureNamed("BaxterThrowRight6I"),
    baxterAtlas.textureNamed("BaxterThrowRight7I"),
    baxterAtlas.textureNamed("BaxterThrowRight8I")]

let baxterLeftThrowFramesI = [baxterAtlas.textureNamed("BaxterThrowLeft1I"),
    baxterAtlas.textureNamed("BaxterThrowLeft2I"),
    baxterAtlas.textureNamed("BaxterThrowLeft3I"),
    baxterAtlas.textureNamed("BaxterThrowLeft4I"),
    baxterAtlas.textureNamed("BaxterThrowLeft5I"),
    baxterAtlas.textureNamed("BaxterThrowLeft6I"),
    baxterAtlas.textureNamed("BaxterThrowLeft7I"),
    baxterAtlas.textureNamed("BaxterThrowLeft8I")]

let heartFlashFrames = [powerAtlas.textureNamed("Heart1"),
    powerAtlas.textureNamed("Heart2")]

let arrowFlashFrames = [powerAtlas.textureNamed("Arrow1"),
    powerAtlas.textureNamed("Arrow2")]

let alcoholFlashFrames = [powerAtlas.textureNamed("Alcohol1"),
    powerAtlas.textureNamed("Alcohol2")]

let starFlashFrames = [powerAtlas.textureNamed("Star1"),
    powerAtlas.textureNamed("Star2")]

let punchSoundEffects = [SKAction.playSoundFileNamed("punch1.mp3",
        waitForCompletion:false),SKAction.playSoundFileNamed("punch2.mp3",
        waitForCompletion:false),SKAction.playSoundFileNamed("punch3.mp3",
        waitForCompletion:false),SKAction.playSoundFileNamed("punch4.mp3", waitForCompletion:false)]