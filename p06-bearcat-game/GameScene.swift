//
//  GameScene.swift
//  p06-bearcat-game
//
//  Created by Vasili Papastrat on 3/1/16.
//  Copyright (c) 2016 Vasili Papastrat. All rights reserved.
//

import SpriteKit
import AVFoundation

class GameScene: SKScene, SKPhysicsContactDelegate {
    let BAXTER_MOVEMENT_SPEED = 3   //how quickly the sprite moves across the screen
    let BAXTER_WALK_ANIMATION_SPEED = 0.06 // how long between each sprite during animation
    
    //MARK: - Data members
    var audioPlayer = AVAudioPlayer()
//    var background = SKSpriteNode(imageNamed: "Street_Example")
    let background1 = SKSpriteNode(imageNamed: "background1")
    let background2 = SKSpriteNode(imageNamed: "background2")
    let lbDistance = SKLabelNode(text: "Distance: 0")
    let lbHighScore = SKLabelNode(text: "High Score: 0")
    let life1 = SKSpriteNode()
    let life2 = SKSpriteNode()
    let life3 = SKSpriteNode()
    let life4 = SKSpriteNode()
    let life5 = SKSpriteNode()
    //var massacre : SKSpriteNode?
    var lives = 3
    
    let player = SKSpriteNode()
    let buttonA = SKSpriteNode(imageNamed: "aButton")
    let buttonB = SKSpriteNode(imageNamed: "bButton")
    let dpad = SKSpriteNode(imageNamed: "dpad")
    var dpadTouchIndicator : SKShapeNode?
    var dpadDown = false    //if user is touching dpad
    var dpadTouchLocation : CGPoint?
    var currentActionKey = ""   //current animation running(ex if walking right, will be 'walkRight')
    var lastLoopTimeInterval : NSTimeInterval = 0//restarts the music when done
    var lastUpdateTimeInterval : NSTimeInterval = 0//time difference between now and last update
    
    var lastSpawnDistance = 0;
    var distance = 0;
    
    let playerCategory: UInt32 = 0x1 << 0
    let projectileCategory: UInt32 = 0x1 << 1
    let enemyCategory: UInt32 = 0x1 << 2
    let powerupCategory: UInt32 = 0x1 << 3
    let borderCategory : UInt32 = 0x1 << 4
    let boundaryCategory: UInt32 = 0x1 << 4
    
    //var basketball : SKSpriteNode? //basketball projectile
    
    var strongerShooter = false
    var strongShots = 0
    var invincible = false
    var invincibleTime : CFTimeInterval = 0
    var currTime : CFTimeInterval = 0
    var distanceThresh = 25
    var enemySpeed : CGFloat = 0.25
    
    /*
    didMoveToView
    
    */
    override func didMoveToView(view: SKView) {
        //set up music
        let url = NSURL.fileURLWithPath(NSBundle.mainBundle().pathForResource("level1", ofType: "mp3")!)
        do{
            try audioPlayer = AVAudioPlayer(contentsOfURL: url, fileTypeHint: nil)
        }catch{
            print("Error with music")
        }
        audioPlayer.play()
        
        //configure labels
        lbDistance.position = CGPoint(x: 100, y: self.view!.frame.height - 50)
        lbDistance.fontColor = SKColor.blackColor()
        
        lbHighScore.position = CGPoint(x:self.view!.frame.width - 100, y: self.view!.frame.height - 50)
        lbHighScore.fontColor = SKColor.blackColor()
        
        //configure background
        /*
        background.position = CGPoint(x: frame.size.width / 2, y:frame.size.height / 2)
        background.size = CGSize(width: frame.width, height: frame.height)
        */
        background1.size = CGSize(width: frame.width*6, height: frame.height)
        background1.anchorPoint = CGPointZero
        background1.position = CGPointMake(0, 0)
        background1.zPosition = -15
        self.addChild(background1)
        
        background2.size = CGSize(width: frame.width*4, height: frame.height)
        background2.anchorPoint = CGPointZero
        background2.position = CGPointMake(background1.size.width,0)
        background2.zPosition = -15
        self.addChild(background2)
        
        //set up lives
        life1.position = CGPoint(x:self.frame.size.width/2 - 60, y: self.frame.size.height - 50)
        life1.size = CGSize(width:30, height:30)
        life1.texture = powerAtlas.textureNamed("Heart1")
        
        life2.position = CGPoint(x:self.frame.size.width/2 - 30, y: self.frame.size.height - 50)
        life2.size = CGSize(width:30, height:30)
        life2.texture = powerAtlas.textureNamed("Heart1")
        
        life3.position = CGPoint(x:self.frame.size.width/2, y: self.frame.size.height - 50)
        life3.size = CGSize(width:30, height:30)
        life3.texture = powerAtlas.textureNamed("Heart1")
        
        life4.position = CGPoint(x:self.frame.size.width/2 + 30, y: self.frame.size.height - 50)
        life4.size = CGSize(width:30, height:30)
        life4.texture = powerAtlas.textureNamed("Heart1")
        life4.hidden = true
        
        life5.position = CGPoint(x:self.frame.size.width/2 + 60, y: self.frame.size.height - 50)
        life5.size = CGSize(width:30, height:30)
        life5.texture = powerAtlas.textureNamed("Heart1")
        life5.hidden = true

        //set up player
        player.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame) - frame.midY/4)
        player.size = CGSize(width: 40, height: 83)
        player.texture = baxterAtlas.textureNamed("BaxterWalkRight1.png")
        player.zPosition = 3
        player.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: player.frame.width, height: player.frame.height - 10))
        player.physicsBody!.usesPreciseCollisionDetection = true
        player.physicsBody!.categoryBitMask = playerCategory
        player.physicsBody!.contactTestBitMask = enemyCategory
        player.physicsBody!.collisionBitMask = borderCategory
        //player.physicsBody!.dynamic = false
        
        //set up buttons
        let buttonAypos = frame.midY - (frame.midY / 1.3)
        let buttonAxpos = frame.midX + (frame.midX / 1.8)
        buttonA.position = CGPointMake(buttonAxpos, buttonAypos)
        buttonA.zPosition = 2
        buttonA.xScale = 0.5
        buttonA.yScale = 0.5
        
        let buttonBypos = frame.midY - (frame.midY / 1.7)
        let buttonBxpos = frame.midX + (frame.midX / 1.2)
        buttonB.position = CGPointMake(buttonBxpos, buttonBypos)
        buttonB.zPosition = 2
        buttonB.xScale = 0.5
        buttonB.yScale = 0.5
        
        //set up dpad
        let dpadXpos = frame.midX - (frame.midX / 1.3)
        let dpadYpos = frame.midY - (frame.midY / 1.6)
        dpad.position = CGPointMake(dpadXpos, dpadYpos)
        dpad.zPosition = 2
        dpad.xScale = 0.5
        dpad.yScale = 0.5
        
        //set up dpad circle touch indicator
        dpadTouchIndicator = SKShapeNode(circleOfRadius: 9.0)
        dpadTouchIndicator!.position = CGPointMake(dpad.position.x, dpad.position.y)
        dpadTouchIndicator!.fillColor = SKColor.whiteColor()
        dpadTouchIndicator!.zPosition = 3
        
        //Set up left boundary
        let boundary = SKSpriteNode()
        boundary.position = CGPoint(x:0, y:self.frame.size.height/2)
        boundary.size = CGSize(width: 1, height: self.frame.size.height)
        boundary.zPosition = 3
        boundary.physicsBody = SKPhysicsBody(rectangleOfSize: boundary.frame.size)
        boundary.physicsBody!.usesPreciseCollisionDetection = true
        boundary.physicsBody!.categoryBitMask = boundaryCategory
        boundary.physicsBody!.contactTestBitMask = enemyCategory
        boundary.physicsBody!.collisionBitMask = 0
        
        //add all to scene
//        self.addChild(background)
        self.addChild(buttonA)
        self.addChild(buttonB)
        self.addChild(player)
        self.addChild(dpad)
        self.addChild(dpadTouchIndicator!)
        self.addChild(lbDistance)
        self.addChild(lbHighScore)
        self.addChild(boundary)
        self.addChild(life1)
        self.addChild(life2)
        self.addChild(life3)
        self.addChild(life4)
        self.addChild(life5)
        
        //Establish current view as physics delegate
        self.physicsBody = SKPhysicsBody(edgeFromPoint: CGPoint(x: 0, y: 0), toPoint: CGPoint(x: self.view!.frame.width,y: 0))
        self.physicsWorld.gravity = CGVectorMake(0, 0)
        self.physicsWorld.contactDelegate = self
        
    }
    /*
    fireBball
        Shoots a basketball from Baxter. Only one basketball
        can be on screen at one time
    PARAMS:
        (BOOL)true if shooting left, false if right
    */
    func fireBball(left left:Bool){
        let basketball = SKSpriteNode(imageNamed: "bball.png")
        basketball.physicsBody = SKPhysicsBody(rectangleOfSize: basketball.size)//use rectangleOfSize??
        basketball.physicsBody!.usesPreciseCollisionDetection = true
        basketball.physicsBody!.categoryBitMask = projectileCategory
        basketball.physicsBody!.collisionBitMask = enemyCategory
        basketball.position = CGPoint(x: player.position.x + 10, y: player.position.y + 10)
        self.addChild(basketball)
        var dx = 100
        if left { // if shooting left
            dx = -100 // change direction because goes right by default
        }
        
        //Ball goes further if powerup is activated
        if (strongerShooter) {
            dx = 160
            if (left) {
                dx = -160
            }
        }
        
        let move = SKAction.moveBy(CGVector(dx: dx, dy: 0), duration: 0.75)
        let remove = SKAction.removeFromParent()
        let sequence = SKAction.sequence([move,remove])
        basketball.runAction(sequence)
        
        //Strong shooter powerup lasts for 5 shots
        if (strongerShooter) {
            strongShots++
            if (strongShots == 5) {
                strongerShooter = false
            }
        }
        //let move = SKAction.moveBy(CGVector(dx: dx, dy: 700), duration: 3)
        basketball.physicsBody!.applyForce(CGVectorMake(100.0, 50.0))
        //basketball!.runAction(move)
    }
    
    //MARK: - Touch reactions
    
    /*
    buttonAtapped
        Called when left button tapped
    */
    func buttonAtapped(){
        print("Button A tapped")
        //if basketball == nil{ //if not a bball on-screen already
            let oldAction = currentActionKey
            clearCurrentAction()
            if oldAction == "walkLeft"{//if facing left
                        //shoot left
                if (invincible) {
                    let throwRightanimation = SKAction.animateWithTextures(baxterLeftThrowFramesI, timePerFrame: 0.03)
                    player.runAction(throwRightanimation,withKey: "throwLeft")
                } else {
                    let throwRightanimation = SKAction.animateWithTextures(baxterLeftThrowFrames, timePerFrame: 0.03)
                    player.runAction(throwRightanimation,withKey: "throwLeft")
                }
                currentActionKey = "throwLeft"
                fireBball(left: true)
            }else{ //otherwise, shoot right
                if (invincible) {
                    let throwRightanimation = SKAction.animateWithTextures(baxterRightThrowFramesI, timePerFrame: 0.03)
                    player.runAction(throwRightanimation,withKey: "throwRight")
                } else {
                    let throwRightanimation = SKAction.animateWithTextures(baxterRightThrowFrames, timePerFrame: 0.03)
                    player.runAction(throwRightanimation,withKey: "throwRight")
                }
                currentActionKey = "throwRight"
                fireBball(left: false)
            }
        //}
    }
    /*
    buttonBtapped
        Called when right button tapped
    */
    func buttonBtapped(){
        print("Button B tapped")
        let oldAction = currentActionKey
        clearCurrentAction()
        if oldAction == "walkLeft"{ //if facing left, punch left
            if (invincible) {
                let punchLeftaction = SKAction.animateWithTextures(baxterPunchLeftFramesI, timePerFrame: 0.05)
                player.runAction(punchLeftaction, withKey: "punchLeft")
            } else {
                let punchLeftaction = SKAction.animateWithTextures(baxterPunchLeftFrames, timePerFrame: 0.05)
                player.runAction(punchLeftaction, withKey: "punchLeft")
            }
            currentActionKey = "punchLeft"
        }else{ //otherwise punch right
            if (invincible) {
                let punchRightaction = SKAction.animateWithTextures(baxterPunchRightFramesI, timePerFrame: 0.05)
                player.runAction(punchRightaction, withKey: "punchRight")
            } else {
                let punchRightaction = SKAction.animateWithTextures(baxterPunchRightFrames, timePerFrame: 0.05)
                player.runAction(punchRightaction, withKey: "punchRight")
            }
            currentActionKey = "punchRight"
        }
        firePunch()
    }
    
    /*
    firePunch
        Creates a temporary invisible projectile to correspond with Baxter's punch
    */
    func firePunch() {
        //Set up left boundary
        let punch = SKSpriteNode()
        if (currentActionKey == "punchRight") {
            punch.position = CGPointMake(player.position.x + player.size.width/2 + 5, player.position.y + player.size.height/4)
        } else {
            punch.position = CGPointMake(player.position.x - player.size.width/2 - 5, player.position.y + player.size.height/4)
        }
        punch.size = CGSize(width: 20, height: player.size.height/2)
        //punch.color = .blackColor()
        punch.physicsBody = SKPhysicsBody(rectangleOfSize: punch.frame.size)
        punch.physicsBody!.usesPreciseCollisionDetection = true
        punch.physicsBody!.categoryBitMask = projectileCategory
        punch.physicsBody!.contactTestBitMask = enemyCategory
        punch.physicsBody!.collisionBitMask = 0
        
        self.addChild(punch)
        
        let punching = SKAction.moveByX(1, y: 0, duration: 0.50)
        let remove = SKAction.removeFromParent()
        let sequence = SKAction.sequence([punching,remove])
        punch.runAction(sequence)
        
    }
    
    /*
    dpadTouched
        Called when user touches anywhere inside dpad.
    PARAMS:
        (Float)x location of user touch
        (Float)y location of user touch
    */
    func dpadTouched(x : CGFloat, y: CGFloat){
        let angle = Float(atan2(y - dpad.position.y, x - dpad.position.x)) //compute angle between touch and dpad's horizontal
        if currentActionKey != "punchRight" && currentActionKey != "punchLeft" && currentActionKey != "throwRight" && currentActionKey != "throwLeft" {
            movePlayer(x, y: y, angle: angle)   //move player according to touch
            if(currentActionKey == "walkRight"){
                let speed = 1//6
//                for _ in 1...speed{
                background1.position = CGPointMake(background1.position.x-(2*CGFloat(speed)), background1.position.y)
                background2.position = CGPointMake(background2.position.x-(2*CGFloat(speed)), background2.position.y)
                if(background1.position.x < -(background1.size.width+(background2.size.width-self.frame.size.width)))
                {
                    background1.position = CGPoint(x:frame.size.width - 20,y:0)
                }
                
                if(background2.position.x < -(background2.size.width+(background1.size.width-self.frame.size.width)))
                {
                    background2.position = CGPoint(x:frame.size.width-20,y:0)
                    
                }
                

                
                //Move powerups along with background
                self.enumerateChildNodesWithName("star", usingBlock: {
                    (node: SKNode!, stop: UnsafeMutablePointer <ObjCBool>) -> Void in
                    node.position.x = node.position.x-(2*CGFloat(speed))
                })
                
                self.enumerateChildNodesWithName("arrow", usingBlock: {
                    (node: SKNode!, stop: UnsafeMutablePointer <ObjCBool>) -> Void in
                    node.position.x = node.position.x-(2*CGFloat(speed))
                })
                
                self.enumerateChildNodesWithName("alcohol", usingBlock: {
                    (node: SKNode!, stop: UnsafeMutablePointer <ObjCBool>) -> Void in
                    node.position.x = node.position.x-(2*CGFloat(speed))
                })
                
                self.enumerateChildNodesWithName("heart", usingBlock: {
                    (node: SKNode!, stop: UnsafeMutablePointer <ObjCBool>) -> Void in
                    node.position.x = node.position.x-(2*CGFloat(speed))
                })
                
                
//                }
/*
                if(background1.position.y < -background1.size.height)
                {
                background1.position = CGPointMake(background2.position.x, background1.position.y + background2.size.height )
                }
                
                if(background2.position.y < -background2.size.height)
                {
                background2.position = CGPointMake(background1.position.x, background2.position.y + background1.size.height)
                
                }
*/
            }
        }
    }
    
    //MARK: - General
    
    /*
    resetPlayerToStand
        Stops current animation and sets to stand
    */
    func resetPlayerToStand(){
        clearCurrentAction()
        if (invincible) {
            player.texture = baxterAtlas.textureNamed("BaxterWalkRight1I.png")
        } else {
            player.texture = baxterAtlas.textureNamed("BaxterWalkRight1.png")
        }

    }
    
    /*
    clearCurrentAction
        If the player is performing an action, stop it
    */
    func clearCurrentAction(){
        if currentActionKey != ""{ //if doing something else
            player.removeActionForKey(currentActionKey) //stop that action
            currentActionKey = ""   //mark as doing nothing
        }
    }
    
    /*
    movePlayer
        Moves the player based on dpad input.
    */
    func movePlayer(x:CGFloat,y:CGFloat,angle:Float){
        //print("angle:,\(angle), x:\(player.position.x), y:\(player.position.y)")
        var moveY = CGFloat(Float(3) * sinf(angle)) //move sprite up
        var moveX = CGFloat(Float(BAXTER_MOVEMENT_SPEED) * cosf(angle))
        
        
        if player.position.y > 150 && angle > 0 { //if dpad is up and player at top boundary
            moveY = 0 //prevent from moving further up
        }
        else if player.position.y < 50 && angle < 0 { //if dpad is down and player at bottom boundary
            moveY = 0 //prevent from moving further down
        }

        if player.position.x < 15 && (angle < -1.5 || angle > 1.5){// if dpad is left and player at left boundary
            moveX = 0 //prevent from moving further left
        }
        distance = distance + Int(moveX)
        if player.position.x > CGFloat(0.75)*(self.view?.bounds.width)! - 15 && angle < 1.5 && angle > -1.5{ // if dpad is right and player at right boundary
            moveX = 0 //prevent from moving further right
        }
        
        let move = SKAction.moveByX(moveX, y: moveY, duration: 0.10)
        
        if moveX < 0 && currentActionKey != "walkLeft"{//if moving left and haven't started animation yet
            clearCurrentAction()
            if (invincible) {
                let walkAction = SKAction.animateWithTextures(walkLeftFramesI, timePerFrame: BAXTER_WALK_ANIMATION_SPEED)
                let walk = SKAction.repeatActionForever(walkAction)
                player.runAction(walk, withKey: "walkLeft")//start animation
            } else {
                let walkAction = SKAction.animateWithTextures(walkLeftFrames, timePerFrame: BAXTER_WALK_ANIMATION_SPEED)
                let walk = SKAction.repeatActionForever(walkAction)
                player.runAction(walk, withKey: "walkLeft")//start animation
            }
            currentActionKey = "walkLeft" //record current animation
        }
        else if moveX > -1 && currentActionKey != "walkRight"{//if moving right and haven't started animation yet
            clearCurrentAction()
            if (invincible) {
                let walkAction = SKAction.animateWithTextures(walkRightFramesI, timePerFrame: BAXTER_WALK_ANIMATION_SPEED)
                let walk = SKAction.repeatActionForever(walkAction)
                player.runAction(walk, withKey: "walkRight")//start animation
            } else {
                let walkAction = SKAction.animateWithTextures(walkRightFrames, timePerFrame: BAXTER_WALK_ANIMATION_SPEED)
                let walk = SKAction.repeatActionForever(walkAction)
                player.runAction(walk, withKey: "walkRight")//start animation
            }
            currentActionKey = "walkRight" //record current animation
        }
        player.runAction(move)
        
    }
    
    /*
    addEnemy
        Adds an enemy to the right hand side of the screen which moves at a particular speed
    */
    func addEnemy(speed:CGFloat) {
        if (distance > lastSpawnDistance) {
            let enemy = SKSpriteNode()
            
            //Generate random y position
            let yPos = CGFloat(arc4random_uniform(110) + 27);
            
            enemy.position = CGPoint(x:self.frame.size.width, y:yPos)
            enemy.name = "enemy"
            enemy.size = CGSize(width: 30, height: 55)
            let mogAtlas = SKTextureAtlas(named:"mog.atlas")
            enemy.texture = mogAtlas.textureNamed("mogStand.png")
            enemy.zPosition = 3
        
            enemy.physicsBody = SKPhysicsBody(rectangleOfSize:
                enemy.frame.size)
            enemy.physicsBody?.usesPreciseCollisionDetection = true
            enemy.physicsBody?.categoryBitMask = enemyCategory
            enemy.physicsBody?.contactTestBitMask = projectileCategory
            enemy.physicsBody?.collisionBitMask = borderCategory
        
            self.addChild(enemy)
            
            //Choose enemy
            var enemyWalkFrames = enemyBoy1WalkFrames
            let enemyNum = arc4random_uniform(6)
            if (enemyNum == 1) {
                enemyWalkFrames = enemyBoy2WalkFrames
            } else if (enemyNum == 2) {
                enemyWalkFrames = enemyBoy3WalkFrames
            } else if (enemyNum == 3) {
                enemyWalkFrames = enemyGirl1WalkFrames
            } else if (enemyNum == 4) {
                enemyWalkFrames = enemyGirl2WalkFrames
            } else if (enemyNum == 5) {
                enemyWalkFrames = enemyGirl3WalkFrames
            }
            
            let move = SKAction.moveByX(-1*speed, y: 0, duration: 0.005)
            let walkAction = SKAction.animateWithTextures(enemyWalkFrames, timePerFrame: 0.1)
            let walk = SKAction.repeatActionForever(walkAction)
            enemy.runAction(walk)
            let moving = SKAction.repeatActionForever(move);
            enemy.runAction(moving)
            
            lastSpawnDistance = distance
        }
    }
    
    /*
    dropPowerup
        Chooses a random powerup to drop in the location at which an enemy was destroyed
    */
    func dropPowerup(x:CGFloat, y:CGFloat) {
        //Randomly determine which powerup will be dropped
        let powerNum = arc4random_uniform(4)
        
        let powerUp = SKSpriteNode()
        
        powerUp.position = CGPoint(x:x, y:y)
        powerUp.size = CGSize(width: 30, height: 30)
        
        //Adjust these lines with powerUp textures (in switch statement based on powerNum)
        if (powerNum == 0) {
            powerUp.color = .whiteColor()
            let flashAction = SKAction.animateWithTextures(heartFlashFrames, timePerFrame: 0.2)
            let flash = SKAction.repeatActionForever(flashAction)
            powerUp.runAction(flash, withKey: "flashing")//start animation
            powerUp.name = "heart"
        } else if (powerNum == 1) {
            powerUp.color = .blueColor()
            let flashAction = SKAction.animateWithTextures(alcoholFlashFrames, timePerFrame: 0.2)
            let flash = SKAction.repeatActionForever(flashAction)
            powerUp.runAction(flash, withKey: "flashing")//start animation
            powerUp.name = "alcohol"
        } else if (powerNum == 2) {
            powerUp.color = .blackColor()
            let flashAction = SKAction.animateWithTextures(arrowFlashFrames, timePerFrame: 0.2)
            let flash = SKAction.repeatActionForever(flashAction)
            powerUp.runAction(flash, withKey: "flashing")//start animation
            powerUp.name = "arrow"
        } else {
            powerUp.color = .redColor()
            let flashAction = SKAction.animateWithTextures(starFlashFrames, timePerFrame: 0.2)
            let flash = SKAction.repeatActionForever(flashAction)
            powerUp.runAction(flash, withKey: "flashing")//start animation
            powerUp.name = "star"
        }
        //let mogAtlas = SKTextureAtlas(named:"mog.atlas")
        //powerUp.texture = mogAtlas.textureNamed("mogStand.png")
        
        powerUp.zPosition = 3
        
        powerUp.physicsBody = SKPhysicsBody(rectangleOfSize:
            powerUp.frame.size)
        powerUp.physicsBody?.usesPreciseCollisionDetection = true
        powerUp.physicsBody?.categoryBitMask = powerupCategory
        powerUp.physicsBody?.contactTestBitMask = playerCategory
        powerUp.physicsBody?.collisionBitMask = 0
        
        self.addChild(powerUp)

    }
    
    //MARK: - Touch Handling
    
    /*
    touchesBegan
        Called when a touch begins
    */
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self) //get touch location
            if dpad.containsPoint(location){       //if touch dpad
                dpadTouchIndicator!.position = CGPointMake(location.x, location.y)//move touch indicator to where user is touching
                dpadTouchLocation = location //update dpadTouchLocation
                dpadDown = true
            }
            else if buttonA.containsPoint(location){ //if button A is touched
                buttonAtapped()
            }else if buttonB.containsPoint(location){ //if button B is touched
                buttonBtapped()
            }
            
        }
    }
    /*
    touchesMoved
        Called when touch moves + continues
        * Doesnt't do anything with the buttons here, only dpad *
    */
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            if dpad.containsPoint(location){   //as user touches within dpad, move touch indicator accordingly
                dpadDown = true
                dpadTouchLocation = location
                dpadTouchIndicator!.position = CGPointMake(location.x, location.y)
            }else{
                //if user moves outside of dpad, reset touch indicator to center of dpad
                dpadTouchIndicator!.position = CGPointMake(dpad.position.x, dpad.position.y)
                dpadDown = false
                if currentActionKey == "walkLeft" || currentActionKey == "walkRight"{//if walking, stop animation
                    resetPlayerToStand()
                }
            }
        }
    }
    
    /*
    touchesEnded
        Called when user picks up finger
    */
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            if dpad.containsPoint(location){ //if user lifted up from dpad
                dpadTouchIndicator!.position = CGPointMake(dpad.position.x, dpad.position.y) //reset touch indicator to center of dpad
                dpadDown = false
                resetPlayerToStand()
            }
            else if buttonA.containsPoint(location){ //if user lifted up from A button
                
            }
            else if buttonB.containsPoint(location){ //if user lifted up from B button
                
            }
        }
    }

    /*
    update
    
    */
    override func update(currentTime: CFTimeInterval) {
        if !player.hasActions(){//if player not doing anything
            self.resetPlayerToStand()
        }
        //if basketball != nil{
        //    if !self.intersectsNode(basketball!){//if bball went off-screen
        //        basketball!.removeFromParent() //remove it from scene
        //        basketball = nil
        //    }
        //}
        if dpadDown{ //if the user is interacting with the dpad
            dpadTouched(dpadTouchLocation!.x, y: dpadTouchLocation!.y)
            //if (currentActionKey == "walkLeft" && player.position.x > 10) {
                //distance--
            //} else if (currentActionKey == "walkRight") {
                //distance++;
            //}
            lbDistance.text = "Distance: \(distance)"
        }
        var timeSinceLast = currentTime - self.lastUpdateTimeInterval//get how much time passed since last update
        self.lastUpdateTimeInterval = currentTime //update data member
        if timeSinceLast > 1{//if more than a second has passed
            timeSinceLast = 1.0 / 60.0
            
        }
        
        if (invincible && (currentTime - invincibleTime) > 30) {
            invincible = false
            print("invincibility expired")
        }
        
        //Enemies spawn faster and move more quickly as distance increases
        if (distance > distanceThresh) {
            addEnemy(enemySpeed)
            distanceThresh += 25
            if (enemySpeed < 0.5) {
                enemySpeed += 0.01
            }
        }
        
        currTime = currentTime
        
        self.updateWithTimeSinceLastUpdate(timeSinceLast)
    }
    
    func updateWithTimeSinceLastUpdate(timeSinceLast : CFTimeInterval){
        self.lastLoopTimeInterval += timeSinceLast
        if self.lastLoopTimeInterval > 106.00{ //restart music
            audioPlayer.pause()
            audioPlayer.currentTime = 0
            audioPlayer.play()
            self.lastLoopTimeInterval = 0//reset
        }
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        print("Contact occurred")
        
        var firstBody = contact.bodyA
        var secondBody = contact.bodyB
        
        if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
        {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        }
        else
        {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        
        //Projectile hits an enemy
        if (firstBody.categoryBitMask == projectileCategory) &&
            (secondBody.categoryBitMask == enemyCategory) {
                print("Shot enemy")
                let punchNumber = Int(arc4random_uniform(4))//randomly choose 1 of 4 sound effects to play
                self.runAction(punchSoundEffects[punchNumber])//play sound effect
                //Determines if powerup is dropped
                let power = arc4random_uniform(3)
                if (power == 1) {
                    dropPowerup(secondBody.node!.position.x, y: secondBody.node!.position.y)
                }
                
                if (secondBody.node != nil) {
                    secondBody.node!.removeFromParent()
                }
                if (firstBody.node != nil) {
                    firstBody.node!.removeFromParent()
                }
                //basketball = nil
        }
        //Enemy runs into player
        else if (firstBody.categoryBitMask == playerCategory) &&
            (secondBody.categoryBitMask == enemyCategory) {
                //Lose? Or Decrease health?
                print("Enemy ran into player")
                if (!invincible) {
                    lives--
                    if (lives == 0) {
                        //Game over
                        let reveal = SKTransition.flipHorizontalWithDuration(0.5)
                        let nextScreen = GameOverScene(size: self.size)
                        self.view!.presentScene(nextScreen, transition: reveal)
                    } else if (lives == 1) {
                        life2.hidden = true
                    } else if (lives == 2) {
                        life3.hidden = true
                    } else if (lives == 3) {
                        life4.hidden = true
                    } else if (lives == 4) {
                        life5.hidden = true
                    }
                }

                secondBody.node!.removeFromParent()
        }
        //Player collects powerup
        else if (firstBody.categoryBitMask == playerCategory) &&
            (secondBody.categoryBitMask == powerupCategory) {
                //Determine which powerup was picked up
                if (secondBody.node!.name == "heart") {
                    print("Extra life!")
                    if (lives < 5) { //Max of 5 lives
                        lives++
                    }
                    
                    if (lives == 2) {
                        life2.hidden = false
                    } else if (lives == 3) {
                        life3.hidden = false
                    } else if (lives == 4) {
                        life4.hidden = false
                    } else if (lives == 5) {
                        life5.hidden = false
                    }
                } else if (secondBody.node!.name == "alcohol") {
                    print("Kill all enemies!")
                    self.enumerateChildNodesWithName("enemy", usingBlock: {
                        (node: SKNode!, stop: UnsafeMutablePointer <ObjCBool>) -> Void in
                        node.removeFromParent()
                    })
                } else if (secondBody.node!.name == "arrow") {
                    print("Special projectile!")
                    strongerShooter = true
                    strongShots = 0
                } else if (secondBody.node!.name == "star") {
                    print("Invisibility!")
                    invincible = true
                    invincibleTime = currTime
                } else {
                    print("Error?")
                }
                secondBody.node!.removeFromParent()
        }
        //Enemy reaches left boundary
        else if (firstBody.categoryBitMask == enemyCategory) &&
            (secondBody.categoryBitMask == boundaryCategory) {
                firstBody.node!.removeFromParent()
                print("Enemy removed")
        }
    }
}
