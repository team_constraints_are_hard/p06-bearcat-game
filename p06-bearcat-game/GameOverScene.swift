//
//  GameOverScene.swift
//  p06-bearcat-game
//
//  Created by Jacob Ruzi on 3/13/16.
//  Copyright © 2016 Vasili Papastrat. All rights reserved.
//

import Foundation
import SpriteKit
import AVFoundation

class GameOverScene : SKScene{
    
    //MARK: - Data members
    var audioPlayer = AVAudioPlayer()
    var lastLoopTimeInterval : NSTimeInterval = 0//restarts the music when done
    var lastUpdateTimeInterval : NSTimeInterval = 0//time difference between now and last update
    let player = SKSpriteNode()
    
    override func didMoveToView(view: SKView) {
        audioPlayer = AVAudioPlayer()
        let url = NSURL.fileURLWithPath(NSBundle.mainBundle().pathForResource("rules", ofType: "mp3")!)
        do{
            try audioPlayer = AVAudioPlayer(contentsOfURL: url, fileTypeHint: nil)
        }catch{
            print("Error with music")
        }
        audioPlayer.play()
        /*let label7 = SKLabelNode(text: "Game over! Click to return to menu!")
        label7.position = CGPoint(x: self.frame.width / 2, y: self.frame.height/2)
        self.addChild(label7)*/
        
        //configure game over image
        let gameOverBackground = SKSpriteNode(imageNamed: "Game_Over")
        gameOverBackground.size = self.frame.size
        gameOverBackground.position = CGPointMake(0, 0)
        gameOverBackground.anchorPoint = CGPointMake(0, 0)
        self.addChild(gameOverBackground)
    }
    
    /*
    update
    
    */
    override func update(currentTime: CFTimeInterval) {
        let punchRightaction = SKAction.animateWithTextures(baxterPunchRightFrames, timePerFrame: 0.05)
        player.runAction(punchRightaction)
        var timeSinceLast = currentTime - self.lastUpdateTimeInterval//get how much time passed since last update
        self.lastUpdateTimeInterval = currentTime //update data member
        if timeSinceLast > 1{//if more than a second has passed
            timeSinceLast = 1.0 / 60.0
            
        }
        self.updateWithTimeSinceLastUpdate(timeSinceLast)
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        goToTitleScreen()
    }
    
    func goToTitleScreen(){
        let reveal = SKTransition.flipHorizontalWithDuration(0.5)
        let nextScreen = TitleScene(size: self.size)
        self.view!.presentScene(nextScreen, transition: reveal)
    }
    
    func updateWithTimeSinceLastUpdate(timeSinceLast : CFTimeInterval){
        self.lastLoopTimeInterval += timeSinceLast
        if self.lastLoopTimeInterval > 20.00{
            if self.audioPlayer.playing{
                self.audioPlayer.stop()
            }
            self.audioPlayer.play()
            self.lastLoopTimeInterval = 0//reset
        }
    }
}