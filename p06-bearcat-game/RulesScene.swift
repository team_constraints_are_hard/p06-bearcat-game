//
//  RulesScene.swift
//  p06-bearcat-game
//
//  Created by Vasili Papastrat on 3/2/16.
//  Copyright © 2016 Vasili Papastrat. All rights reserved.
//

import Foundation
import SpriteKit
import AVFoundation

class RulesScene : SKScene{
    
    //MARK: - Data members
    var audioPlayer = AVAudioPlayer()
    var lastLoopTimeInterval : NSTimeInterval = 0//restarts the music when done
    var lastUpdateTimeInterval : NSTimeInterval = 0//time difference between now and last update
    let player = SKSpriteNode()

    override func didMoveToView(view: SKView) {
        audioPlayer = AVAudioPlayer()
        let url = NSURL.fileURLWithPath(NSBundle.mainBundle().pathForResource("rules", ofType: "mp3")!)
        do{
            try audioPlayer = AVAudioPlayer(contentsOfURL: url, fileTypeHint: nil)
        }catch{
            print("Error with music")
        }
        audioPlayer.play()
        let numRules: CGFloat = 10
        let label1 = SKLabelNode(text: "How to play:")
        label1.position = CGPoint(x: self.frame.width / 2, y: self.frame.height - (self.frame.height/numRules))
        self.addChild(label1)
        let label2 = SKLabelNode(text: "Move using the circle as directional controller")
        label2.position = CGPoint(x: self.frame.width / 2, y: self.frame.height - 2*(self.frame.height/numRules))
        self.addChild(label2)
        let label3 = SKLabelNode(text: "Shoot basketballs using A")
        label3.position = CGPoint(x: self.frame.width / 2, y: self.frame.height - 3*(self.frame.height/numRules))
        self.addChild(label3)
        let label4 = SKLabelNode(text: "Punch enemies using B")
        label4.position = CGPoint(x: self.frame.width / 2, y: self.frame.height - 4*(self.frame.height/numRules))
        self.addChild(label4)
        let label5 = SKLabelNode(text: "DO NOT let enemies hurt you!")
        label5.fontColor = UIColor.redColor()
        label5.position = CGPoint(x: (self.frame.width / 4)+25, y: self.frame.height - 5*(self.frame.height/numRules))
        self.addChild(label5)
        let label6 = SKLabelNode(text: "DO destroy enemies!")
        label6.fontColor = UIColor.greenColor()
        label6.position = CGPoint(x: self.frame.width / 1.25, y: self.frame.height - 5*(self.frame.height/numRules))
        self.addChild(label6)
        let label7 = SKLabelNode(text: "Click anywhere to return home")
        label7.position = CGPoint(x: self.frame.width / 2, y: self.frame.height - 8*(self.frame.height/numRules))
        self.addChild(label7)
        player.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame) - frame.midY/4)
        player.size = CGSize(width: 40, height: 83)
        player.texture = baxterAtlas.textureNamed("BaxterWalkingRight1.png")
        self.addChild(player)

        
    }
    
    /*
    update
    
    */
    override func update(currentTime: CFTimeInterval) {
        let punchRightaction = SKAction.animateWithTextures(baxterPunchRightFrames, timePerFrame: 0.05)
        player.runAction(punchRightaction)
        var timeSinceLast = currentTime - self.lastUpdateTimeInterval//get how much time passed since last update
        self.lastUpdateTimeInterval = currentTime //update data member
        if timeSinceLast > 1{//if more than a second has passed
            timeSinceLast = 1.0 / 60.0
            
        }
        self.updateWithTimeSinceLastUpdate(timeSinceLast)
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        goToTitleScreen()
    }
    
    func goToTitleScreen(){
        let reveal = SKTransition.flipHorizontalWithDuration(0.5)
        let nextScreen = TitleScene(size: self.size)
        self.view!.presentScene(nextScreen, transition: reveal)
    }
    
    func updateWithTimeSinceLastUpdate(timeSinceLast : CFTimeInterval){
        self.lastLoopTimeInterval += timeSinceLast
        if self.lastLoopTimeInterval > 20.00{
            if self.audioPlayer.playing{
                self.audioPlayer.stop()
            }
            self.audioPlayer.play()
            self.lastLoopTimeInterval = 0//reset
        }
    }
}